package com.example.healthkit

import android.R
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AlarmScreenTest {

    @Test
    fun clickAlarmButtoonAfterFillingForm_showProgressAndSuccessAndSuccessScreen(){
        launchFragmentInContainer<SetReminder>()
        val medicinName = "Name"
        val medicinType = "Type"
        val time = "Time"
        val description = "Description"

        Espresso.onView(ViewMatchers.withId(com.example.healthkit.R.id.medicine_name))
            .perform(typeText(medicinName))

        Espresso.onView(ViewMatchers.withId(com.example.healthkit.R.id.medicine_type))
            .perform(typeText(medicinType))

        Espresso.onView(ViewMatchers.withId(com.example.healthkit.R.id.medicine_desc))
            .perform(typeText(description))

        Espresso.onView(ViewMatchers.withId(com.example.healthkit.R.id.time))
            .perform(typeText(time))

        Espresso.onView(ViewMatchers.withId(com.example.healthkit.R.id.save_reminder_btn)).perform(click())

        onView(withId(com.example.healthkit.R.id.save_reminder_btn)).check(ViewAssertions.matches(withId(com.example.healthkit.R.id.save_reminder_btn)))

    }

}