package com.example.healthkit

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeFragmentTest  {

    @Test
    fun launchFragmentHome(){
        launchFragmentInContainer<HomeFragment>()

        // now use espresso to look for the fragment's text view and verify it is displayed
        onView(withId(R.id.welcome_text_view)).check(matches(withText("Welcome to HealthKit App")))   }
}