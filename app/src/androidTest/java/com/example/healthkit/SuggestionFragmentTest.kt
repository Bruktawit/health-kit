package com.example.healthkit

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText


@RunWith(AndroidJUnit4::class)
class SuggestionFragmentTest {

    @Test
    fun launchFragmentSuggestion(){

        launchFragmentInContainer<SuggestionFragment>()

        onView(withId(R.id.editText)).check(matches(withId(R.id.editText)))

    }
}