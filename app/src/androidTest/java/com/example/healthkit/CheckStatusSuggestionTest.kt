package com.example.healthkit

import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.allOf
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CheckStatusSuggestionTest {

    fun clickCheckStatusAfterFillingForm_showSuggestionScreen() {


        val afc = "AFC"
        val bcc = "BCC"
        val gcc = "GCC"

        //find the firstname edit text and type in the first name
        onView(withId(R.id.editText)).perform(typeText(afc))

        //find the lastname edit text and type in the last name
        onView(withId(R.id.editText2)).perform(typeText(bcc))

        //find the email address edit text and type in the email address
        onView(withId(R.id.editText3)).perform(typeText(gcc))

        //click the signup button
        onView(withId(R.id.checkbox)).perform(click())

        //check that we can see the success screen with success message
        val successString = InstrumentationRegistry.getTargetContext().getString(1)
        onView(withId(R.id.text)).check(matches(allOf(withText(successString), isDisplayed())))
    }
}