package com.example.healthkit.Data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.healthkit.Dao.ReminderDao


@Database(entities = arrayOf(Suggestion::class, Tip::class),version = 1)

abstract class MyDatabase: RoomDatabase() {

    abstract fun suggestionDao():SuggestionDAO
    abstract fun tipDao(): TipDAO
    abstract fun reminderDao():ReminderDao

    companion object {

        @Volatile
        private var INSTANCE: MyDatabase? = null

        fun getDatabase(context: Context): MyDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java, "my_database"
                ).build()

                INSTANCE = instance
                return instance
            }

        }
    }
}