package com.example.healthkit

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.TimePicker
import androidx.core.content.ContextCompat.getSystemService
import java.util.*

class ReminderAlarm {
    lateinit var alarmManager: AlarmManager
    lateinit var timePicker: TimePicker
    lateinit var setOffButton: Button
    lateinit var saveButton: Button
    lateinit var reminderText: TextView
    lateinit var context: Context
    lateinit var pendingIntent: PendingIntent
    var hour: Int = 0
    var minute: Int = 0
    val reminderViewModel = ViewModelProviders.of(this).get(ReminderViewModel::class.java)
    private fun setAlarmText(s: String) {
        reminderText.setText(s)
    }
    private fun reminderAlarm(){
        this.context = this
        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        timePicker = findViewById(R.id.time_picker) as TimePicker
        saveButton = findViewById(R.id.save_reminder_btn)
        setOffButton = findViewById(R.id.set_off_btn)
        reminderText = findViewById(R.id.reminder_view)
        var calander: Calendar = Calendar.getInstance()
        var intent = Intent(this, AlarmReceiver::class.java)



        saveButton.setOnClickListener(object : View.OnClickListener {
            if (connected()) {

                reminderViewModel.insertReminder.observe(this, Observer { response ->
                    response.body()?.run{
                        updateFields(this)
                    }
                    updateStatusMessageField(response.code().toString())
                })
            }
            else{
                reminderViewModel.saveToLocal(reminder)
            }
            override fun onClick(p0: View?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    calander.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                    calander.set(Calendar.MINUTE, timePicker.minute)
                    calander.set(Calendar.SECOND, 0)
                    calander.set(Calendar.MILLISECOND, 0)
                    hour = timePicker.hour
                    minute = timePicker.minute


                } else {
                    calander.set(Calendar.HOUR_OF_DAY, timePicker.currentHour)
                    calander.set(Calendar.MINUTE, timePicker.currentMinute)
                    calander.set(Calendar.SECOND, 0)
                    calander.set(Calendar.MILLISECOND, 0)
                    hour = timePicker.currentHour
                    minute = timePicker.currentMinute


                }
                var hour_string: String = hour.toString()
                var minute_string: String = minute.toString()
                if (hour > 12) {
                    hour_string = (hour - 12).toString()

                }
                if (minute < 10) {
                    minute_string = "0$minute"
                }
                setAlarmText("Alarm set to:$hour_string:$minute_string")
                intent.putExtra("extra", "on")
                pendingIntent = PendingIntent.getBroadcast(this@MainActivity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calander.timeInMillis, pendingIntent)

            }


        })
        setOffButton.setOnClickListener(object : View.OnClickListener {
            if (connected()) {

                reminderViewModel.deleteReminder.observe(this, Observer { response ->
                    response.body()?.run{
                        updateFields(this)
                    }
                    updateStatusMessageField(response.code().toString())
                })
            }

            override fun onClick(p0: View?) {
                intent.putExtra("extra", "off")
                pendingIntent = PendingIntent.getBroadcast(this@MainActivity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                alarmManager.cancel(pendingIntent)
                sendBroadcast(intent)
                setAlarmText("Alarm Off")
            }
        })
    }
    private fun connected():Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected
    }
    private fun updateFields(reminder:Reminder){
        save.run{
            reminder.setText("")
        }

}