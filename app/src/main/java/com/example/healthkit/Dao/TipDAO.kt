package com.example.healthkit.Data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TipDAO {

    @Query("SELECT * FROM Tip WHERE tipFor=:tipForValue")
    fun getTipFor(tipForValue: String):Tip

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    fun saveTip(tip: Tip)
}