package com.example.healthkit.Dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.healthkit.Data.Reminder
import com.example.healthkit.Data.Suggestion

@Dao
interface ReminderDao {
    @Query("SELECT * FROM   reminders ORDER BY medicine_name")
    fun findAllReminders():LiveData<List<Reminder>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReminder(reminder:Reminder):Long

    @Query("SELECT * FROM reminders WHERE medicine_name=:name")
    fun getReminderByName(name: String): Reminder

}