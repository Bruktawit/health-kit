package com.example.healthkit.Data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface SuggestionDAO {

    @Query("SELECT * FROM Suggestion WHERE tag=:tagValue")
    fun getSuggestionByTag(tagValue: String):Suggestion

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    fun saveSuggestion(suggestion: String)
}