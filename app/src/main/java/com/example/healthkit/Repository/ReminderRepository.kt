package com.example.healthkit.Repository


import androidx.lifecycle.LiveData
import com.example.healthkit.Dao.ReminderDao
import com.example.healthkit.Data.Reminder
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class ReminderRepository(val reminderDao: ReminderDao){
    private val reminderApiService=ApiService.getInstance()
    suspend fun findAll(): Response<List<Reminder>> =
            withContext(Dispatchers.IO){
                reminderApiService.findAll().await()
            }
    suspend fun addReminder(reminder:Reminder): Response<Reminder> =
            withContext(Dispatchers.IO) {
                reminderApiService.addReminder(reminder).await()

            }
    suspend fun deleteReminder(id:Long,newReminder: Reminder): Response<Void> =
        withContext(Dispatchers.IO){
            reminderApiService.deleteReminder(id).await()
        }
    suspend fun updateReminder(id: Long,newReminder:Reminder): Response<Reminder> =
        withContext(Dispatchers.IO) {
            reminderApiService.updateReminder(id,newReminder).await()

        }
    fun saveToLocal(reminder: Reminder){
        reminderDao.insertReminder(reminder)
    }
    fun getReminderLocal(){
        reminderDao.findAllReminders()
    }
}
