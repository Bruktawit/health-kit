package com.example.healthkit.Repository



import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.SuggestionDAO
import com.example.healthkit.Network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SuggestionRepository(private val suggestionApiService:SuggestionApiService,private val suggestionDAO: SuggestionDAO) {

    suspend fun getSuggestion(tag: String): Response<Post> =
            withContext(Dispatchers.IO){
                suggestionApiService.findByTagAsync(tag).await()
            }

    suspend fun insertSuggestion(suggestion:Suggestion): Response<Suggestion> =
            withContext(Dispatchers.IO) {
                suggestionApiService.saveSuggestion(suggestion).await()
            }


    fun saveToLocal(suggestion:Suggestion){
        suggestionDAO.saveSuggestion(suggestion.tag)
    }

    fun getSuggestionByTagLocal(tag:String): Suggestion{
        return suggestionDAO.getSuggestionByTag(tag)
    }
}