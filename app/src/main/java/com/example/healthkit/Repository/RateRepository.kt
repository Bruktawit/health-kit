package com.example.healthkit.Repository


import com.example.healthkit.Data.Rate
import com.example.healthkit.Network.ApiService


class RateRepository( private val suggestionApiService:SuggestionApiService,private val suggestionDao:SuggestionDao) {



    suspend fun deleteRate(userRate:Int): Response<Void> =
            withContext(Dispatchers.IO){
                rateApiService.deleteRate(userRate).await()
            }
    suspend fun saveRate(rateSaved:Rate): Response<Post> =
            withContext(Dispatchers.IO) {
                rateApiService.saveRateAsync(rateSaved).await()
            }
    fun deleteRateLocal(userRate:Int){
        rateDao.saveRate(rateSaved:Rate)
    }
    fun saveRateLocal(rateSaved:Rate){
        rateDao.saveRate(rateSaved:Rate)
    }
}