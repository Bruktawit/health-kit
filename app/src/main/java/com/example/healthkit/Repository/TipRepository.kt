package com.example.healthkit.Repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.healthkit.Data.Tip
import com.example.healthkit.Data.TipDAO
import com.example.healthkit.Network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TipRepository(private val tipApiService:TipApiService,private val tipDAO: TipDAO) {

    suspend fun saveTip(tip:Tip): Response<Tip> =
            withContext(Dispatchers.IO) {
                TipApiService.insertPost(tip).await()
            }
    suspend fun getTipByTipFor(tipFor:String): Response<List<Tip>> =
            withContext(Dispatchers.IO){
                tipApiService.findByTipForAsync(tipFor).await()
            }

    fun saveToLocal(tip: Tip){
        tipDAO.saveTip(tip)
    }

    fun getTipByTipForLocal(tipFor:String): Tip {
        return tipDAO.getTipFor(tipFor)
    }

}