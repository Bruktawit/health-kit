package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.healthkit.ViewModel.TipViewModel
import com.example.healthkit.databinding.FragmentTipBinding
import kotlinx.android.synthetic.main.fragment_tip.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TipFragment : Fragment() {

    lateinit var fragmentTipViewModel: TipViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

         activity?.let{
             fragmentTipViewModel=ViewModelProviders.of(it).get(TipViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentTipBinding: FragmentTipBinding =
            DataBindingUtil.inflate(inflater,R.layout.fragment_tip,container,false)
        val view=fragmentTipBinding.root

        fragmentTipBinding.viewmodel=fragmentTipViewModel
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        pre_button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.toGetTip))
        dia_button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.toGetTip))
    }
}
