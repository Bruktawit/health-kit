package com.example.healthkit

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.healthkit.Data.Reminder
import kotlinx.android.synthetic.main.fragment_item_in_list.view.*

class ReminderListAdapter(context: Context):RecyclerView.Adapter<ReminderListAdapter.ReminderViewHolder>() {

    private val inflater=LayoutInflater.from(context)

    private var reminders= listOf(Reminder)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderViewHolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val recyclerViewItem=inflater.inflate(R.layout.fragment_item_in_list,parent,false)
         return ReminderViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
       reminders.size
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: ReminderViewHolder, position: Int) {
        val reminder=reminders[position]
        holder.reminderMedicineName.text=reminder.medicine_name

    }

    inner class ReminderViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val reminderMedicineName=itemView.tv_mname
    }
}