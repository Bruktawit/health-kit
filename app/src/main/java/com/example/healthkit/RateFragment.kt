package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.healthkit.ViewModel.RateViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RateFragment : Fragment() {

    lateinit var fragmentTipViewModel: RateViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let{
            fragmentTipViewModel= ViewModelProviders.of(it).get(RateViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentTipBinding:TipFragment=
            DataBindingUtil.inflate(inflater,R.layout.fragment_rate,container,false)
        val view=fragmentTipBinding.root

        fragmentTipBinding.viewmodel=fragmentTipViewModel
        return view
    }



}
