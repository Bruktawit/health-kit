package com.example.healthkit.ViewModel

import android.app.Application
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.Tip
import com.example.healthkit.Repository.TipRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TipViewModel:ViewModel {

    private val tipRepository: TipRepository
    lateinit var tip: Tip



    init {
       // val rateDao= MyDatabase.getDatabase(application).rateDao()
        val tipApiService=TipApiService.getInstance()
        //rateRepositoryLocal=RateRepository(rateDao)
        tipRepository=TipRepository(tipApiService)
    }

    private val _getResponse = MutableLiveData<Response<Tip>>()
    val getResponse: LiveData<Response<Tip>>
        get() = _getResponse
    private val _insertResponse = MutableLiveData<Response<Tip>>()
    val insertResponse: LiveData<Response<Tip>>
        get() = _insertResponse

    fun insertTip(tip:Tip) = viewModelScope.launch {
        _insertResponse.postValue(tipRepository.saveTip(tip))
    }

    fun getTip(tipFor:String) = viewModelScope.launch {
        _getResponses.postValue(tipRepository.getTipByTipFor(tipFor))
    }

    fun getTipLocal(tip: String) {

        suggestionRepository.getTipByTipForLocal(tag)
    }

    fun insertTipLocal(tip:Tip) {

        suggestionRepository.saveToLocal(tip)
    }

}