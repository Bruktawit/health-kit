package com.example.healthkit.ViewModel


import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Repository.SuggestionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SuggestionViewModel(application: Application): AndroidViewModel(application), Observable {

    private val suggestionRepository: SuggestionRepository
    lateinit var suggestion: Suggestion

    val suggestionTitle = MutableLiveData<String>()
    val suggestionDescription = MutableLiveData<String>()

    init {
        val suggestionDao = MyDatabase.getDatabase(application).suggestionDao()
        suggestionRepository = SuggestionRepository(suggestionDao)
        saveToLocal()
    }

    private val _getResponse = MutableLiveData<Response<Post>>()
    val getResponse: LiveData<Response<Post>>
        get() = _getResponse
    private val _insertResponse = MutableLiveData<Response<Post>>()
    val insertResponse: LiveData<Response<Post>>
        get() = _insertResponse

    fun insertSuggestion(suggestion: Suggestion) = viewModelScope.launch {
        _insertResponse.postValue(suggestionRepository.insertSuggestion(suggestion))
    }

    fun getSuggestion(id: Long) = viewModelScope.launch {
        _getResponses.postValue(suggestionRepository.getSuggestion(tag))
    }

    fun getSuggestionLocal(tag: String) {

        suggestionRepository.getSuggestionByTagLocal(tag)
    }

    fun insertSuggestion(suggestion: Suggestion) {

        suggestionRepository.saveToLocal(suggestion)
    }

}




    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}