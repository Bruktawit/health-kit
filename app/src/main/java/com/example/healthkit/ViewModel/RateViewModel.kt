package com.example.healthkit.ViewModel

import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.healthkit.Data.Rate
import com.example.healthkit.Repository.RateRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RateViewModel:ViewModel{

    private val rateRepository: RateRepository

    init {
        val rateDao= MyDatabase.getDatabase(application).rateDao()
        val rateApiService=RateApiService.getInstance()
        rateRepositoryLocal=RateRepository(rateDao)
        rateRepository=RateRepository(rateApiService)
    }
    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse
    private val _insertResponse = MutableLiveData<Response<Rate>>()
    val insertResponse: LiveData<Response<Rate>>
        get() = _insertResponse
    fun insertRate(rate:Rate) = viewModelScope.launch {
        _insertResponse.postValue(rateRepository.saveRate(rate))
    }

    fun deleteRate(userRate: Int) = viewModelScope.launch {
        _deleteResponse.postValue(rateRepository.deleteRate(userRate))
    }
    fun deleteRateLocal(userRate:Int){

        rateRepository.deleteRateLocal(userRate)

    }
    fun saveRateLocal(rateSaved:Rate){

        rateRepository.saveRateLocal(rateSaved)

    }
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}