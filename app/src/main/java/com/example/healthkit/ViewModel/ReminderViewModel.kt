package com.example.healthkit.ViewModel

import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.*
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Reminder
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Repository.ReminderRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ReminderViewModel(application: Application):AndroidViewModel(application), Observable {
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val rateRepository: RateRepository

    init {
        val rateDao= MyDatabase.getDatabase(application).rateDao()
        val rateApiService=RateApiService.getInstance()
        rateRepositoryLocal=RateRepository(rateDao)
        rateRepository=RateRepository(rateApiService)
    }

    private  val _getResponse = MutableLiveData<Response<Post>>()
    val getResponse: LiveData<Response<Post>>
        get() = _getResponse


    private val _updateResponse = MutableLiveData<Response<Post>>()
    val updateResponse: LiveData<Response<Post>>
        get() = _updateResponse
    private val _insertResponse = MutableLiveData<Response<Post>>()
    val insertResponse: LiveData<Response<Post>>
        get() = _insertResponse
    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse


    fun updateReminder(id: Long,reminder:Reminder) = viewModelScope.launch {
        _updateResponse.postValue(reminderRepository.updateReminder(id, reminder))

    }
    fun insertReminder(reminder:Reminder) = viewModelScope.launch {
        _insertResponse.postValue(reminderRepository.addReminder(reminder))
    }

    fun deleteReminder(id: Long,reminder:Reminder) = viewModelScope.launch {
        _deleteResponse.postValue(reminderRepository.deleteReminder(id,reminder))
    }

    fun getReminder(id: Long) = viewModelScope.launch {
        _getResponses.postValue(reminderRepository.findAll())
    }

    fun saveToLocal(){
             reminderRepository.saveToLocal(reminder)

    }
    fun getReminder(){
       reminderRepository.getReminderLocal()
    }







}