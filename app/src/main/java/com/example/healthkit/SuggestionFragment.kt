package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.healthkit.ViewModel.SuggestionViewModel
import com.example.healthkit.databinding.SuggestionBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SuggestionFragment : Fragment() {

    lateinit var fragmentSuggestionViewModel: SuggestionViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let{
            fragmentSuggestionViewModel=ViewModelProviders.of(it).get(SuggestionViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentSuggestionBinding:SuggestionBinding =
            DataBindingUtil.inflate(inflater,R.layout.fragment_suggestion,container,false)
        val view=fragmentSuggestionBinding.root

        fragmentSuggestionBinding.viewmodel.fragmentSuggestionViewModel
        return view
    }


}
