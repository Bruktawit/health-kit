package com.example.healthkit.Data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Tip(@PrimaryKey(autoGenerate = true) val id:Long, val tipFor:String, val tip:String) {
}