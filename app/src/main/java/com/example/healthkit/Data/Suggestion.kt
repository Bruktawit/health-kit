package com.example.healthkit.Data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Suggestion(@PrimaryKey(autoGenerate = true) val id:Long, val tag:String, val description:String){

}