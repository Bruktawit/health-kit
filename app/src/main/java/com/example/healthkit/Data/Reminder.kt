package com.example.healthkit.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName="reminders")
data class Reminder(@ColumnInfo(name="medicine_name") var medicine_name:String,
                    @ColumnInfo(name="medicine_type")var medicine_type:String,
                    @ColumnInfo(name="desc")var description:String):Serializable


