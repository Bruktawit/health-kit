package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR.viewModel
import androidx.navigation.fragment.findNavController
import com.example.healthkit.ViewModel.TipViewModel

import com.example.healthkit.databinding.FragmentListReminderBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ListReminder : Fragment() {

    lateinit var fragmentListReminderViewModel: TipViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val button = view?.findViewById<Button>(R.id.add_btn)
        button?.setOnClickListener {
            findNavController().navigate(R.id.set_reminder, null)
            if (connected()) {
                reminderViewModel.getReminder(id)
                reminderViewModel.getReminder.observe(this, Observer { response ->
                    response.body()?.run{
                        //updateFields(this)
                    }
                    updateStatusMessageField(response.code().toString())
                })
            }
            else{

                reminderViewModel.getReminder(id)
            }
        }

        // Inflate the layout for this fragment
        val fragmentListReminderBinding:com.example.healthkit.databinding.ListDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_suggestion, container, false)


        fragmentListReminderBinding.viewmodel = FragmentListReminderBinding


    }

}


