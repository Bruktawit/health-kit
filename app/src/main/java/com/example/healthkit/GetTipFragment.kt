package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.healthkit.ViewModel.TipViewModel
import com.example.healthkit.databinding.FragmentGetTipBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class GetTipFragment : Fragment() {

    lateinit var fragmentGetTipViewModel: TipViewModel
    val tipFor=findById(R.id.tipFor).toString()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let{
            fragmentGetTipViewModel= ViewModelProviders.of(it).get(TipViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentGetTipBinding: FragmentGetTipDataBinding =
            DataBindingUtil.inflate(inflater,R.layout.fragment_get_tip,container,false)
        val view=fragmentGetTipBinding.root

        fragmentGetTipBinding.viewmodel=fragmentGetTipViewModel
        if(connected()){

            TipViewModel.getTip.observe(this, Observer{response ->
                val owner = this
                response.body()?.run{
                    var news = readFields(this)

                    tipViewModel.getTip(tipFor)
                    newsViewModel.insertResponse.observe(owner, Observer { response ->
                        response.body()?.run{

                        }
                    })
                }

            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
        return view
    }


}
