package com.example.healthkit.Network

import com.example.healthkit.Data.Rate
import com.example.healthkit.Data.Reminder
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.Tip
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ApiService {

    @GET("suggestions/{tag}")
    fun findByTagAsync(@Path("tag") tag: String): Call<Suggestion>
    @POST("rate/new")
    fun saveSuggestion(@Body suggestion:Suggestion): Call<Rate>

    @GET("tips/{tipFor}")
    fun findByTipForAsync(@Path("tipFor") tipFor:String):Call<Tip>

    @POST("tip/new")
    fun saveTip(@Path("saveFor") saveFor:String,@Body newTip:Tip): Deferred<Response<Tip>>

    @POST("rate/new")
    fun saveRateAsync(@Body rateSaved: Rate): Call<Rate>

    @GET("reminder/all")
    fun findAll(): Deferred<Response<List<Reminder>>>

    @POST("reminders")
    fun addReminder(@Body reminder: Reminder): Deferred<Response<Reminder>>

    @DELETE("/{userRate}")
    fun deleteRate(@Path("userRate") userRate: Int): Call<Void>

    @POST("reminders")
    fun updateReminder(@Path("id") id:Long,@Body newReminder:Reminder): Deferred<Response<Reminder>>

    @DELETE("reminders/{id}")
    fun deleteReminder(@Path("id") id:Long):Deferred<Response<Void>>

    companion object {

        private val baseUrl = "http://localhost:9090/api/"


        fun getInstance(): ApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}