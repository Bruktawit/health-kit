package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.healthkit.ViewModel.SuggestionViewModel
import com.example.healthkit.databinding.CheckStatusBinding


import kotlinx.android.synthetic.main.fragment_check_status.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CheckStatusFragment : Fragment() {

    lateinit var fragmentViewModel: SuggestionViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let{
            fragmentViewModel=ViewModelProviders.of(it).get(SuggestionViewModel::class.java)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

         val fragmentCheckBinding:com.example.healthkit.databinding.CheckStatusBinding=DataBindingUtil.inflate(inflater,R.layout.fragment_check_status,container,false)
         val view=fragmentCheckBinding.root

        fragmentCheckBinding.viewmodel=fragmentViewModel
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        check_button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.toSuggestion))
    }
    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
}
