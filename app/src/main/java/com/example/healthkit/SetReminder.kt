package com.example.healthkit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.healthkit.ViewModel.TipViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SetReminder : Fragment() {

    lateinit var fragmentSetReminderViewModel: TipViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentSuggestionBinding: com.example.healthkit.databinding.FragmentSetReminderBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_set_reminder,container,false)
        val view=fragmentSuggestionBinding.root

        fragmentSuggestionBinding.viewmodel=fragmentSetReminderViewModel
        return view
    }


}
