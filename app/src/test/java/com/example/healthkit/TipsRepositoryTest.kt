package com.example.healthkit.Data.viewModel

import com.example.healthkit.Data.Tip
import com.example.healthkit.Repository.TipRepository

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.SuggestionDAO
//import com.example.healthkit.Database.MyDatabase
import com.example.healthkit.Repository.SuggestionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest

class TipsRepositoryTest {

    private lateinit var repo: TipRepository
    private lateinit var database: MyDatabase

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            MyDatabase::class.java,
            "tip").allowMainThreadQueries().build()

        repo = TipRepository(database.tipDao())
    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new tip saved in the database
        val tip = Tip(1, "diabetic","take this medicine" )
        repo.saveToLocal(tip)

        // WHEN  - tip retrieved by tag
        val result  = repo.getTip("diabetes")

        Assert.assertThat(result, CoreMatchers.notNullValue())

        // THEN - Same suggesion is returned
//        Assert.assertThat(result.id, CoreMatchers.`is`("1"))
//        Assert.assertThat(result.tag, CoreMatchers.`is`("Normal"))
//        Assert.assertThat(result.description, CoreMatchers.`is`("U are normal"))

    }


}


