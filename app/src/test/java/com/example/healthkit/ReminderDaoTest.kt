package com.example.healthkit.Data.data


import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Reminder
import kotlinx.coroutines.ExperimentalCoroutinesApi
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ReminderDaoTest {

    private lateinit var database: MyDatabase

    @Before
    fun initDb() {

        database =
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                MyDatabase::class.java,
                "reminder"
            ).allowMainThreadQueries().build()


    }

    @After
    fun closeDb() = database.close()


    @Test
    fun InsertRemainder(){
        // GIVEN - insert a group
        val reminder = Reminder("Amoksilin", "pill", "made in ethiopia")
        database.reminderDao().insertReminder(reminder)

        // WHEN - Get the group by code from the database
        val loaded = database.reminderDao().getReminderByName(name = "Bety")

        MatcherAssert.assertThat(loaded.medicine_name, CoreMatchers.`is`(reminder.medicine_name))
        MatcherAssert.assertThat(loaded.medicine_type, CoreMatchers.`is`(reminder.medicine_type))
        MatcherAssert.assertThat(loaded.description, CoreMatchers.`is`(reminder.description))
    }

    @Test
    fun findAllRemaindersTest() {  // = runBlockingTest
        val reminder = Reminder("AA", "Amoksilin", "take it at 5.")
        database.reminderDao().findAllReminders()

        val result = database.reminderDao().findAllReminders()

        MatcherAssert.assertThat(result, CoreMatchers.notNullValue())
    }

}
