package com.example.healthkit.Data.viewModel


import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Reminder
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.SuggestionDAO
import com.example.healthkit.Repository.ReminderRepository
//import com.example.healthkit.Database.MyDatabase
import com.example.healthkit.Repository.SuggestionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest

class ReminderRepositoryTest {

    private lateinit var repo: ReminderRepository
    private lateinit var database: MyDatabase

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            MyDatabase::class.java,
            "reminder").allowMainThreadQueries().build()

        repo = ReminderRepository(database.reminderDao())
    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new remainder saved in the database
        val remainder = Reminder("name", "type","description" )
        repo.saveToLocal(remainder)

        // WHEN  - remainder retrieved
        val result  = repo.getReminderLocal()

        Assert.assertThat(result, CoreMatchers.notNullValue())

        // THEN - Same suggesion is returned
//        Assert.assertThat(result.id, CoreMatchers.`is`("1"))
//        Assert.assertThat(result.tag, CoreMatchers.`is`("Normal"))
//        Assert.assertThat(result.description, CoreMatchers.`is`("U are normal"))

    }


}


