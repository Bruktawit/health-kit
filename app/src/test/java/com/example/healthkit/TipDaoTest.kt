package com.example.healthkit.Data.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Suggestion
import com.example.healthkit.Data.Tip
import kotlinx.coroutines.ExperimentalCoroutinesApi
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class TipDaoTest {

    private lateinit var database: MyDatabase

    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database =
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                MyDatabase::class.java,
                "tip"
            ).allowMainThreadQueries().build()


    }

    @After
    fun closeDb() = database.close()


    @Test
    fun saveTipAndGetTipForTest(){
        // GIVEN - insert a group
        val tip = Tip(1, "diabetes1", "eat healthy food.")
        database.tipDao().saveTip(tip)

        // WHEN - Get the group by code from the database
        val loaded = database.tipDao().getTipFor("diabetes1")

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat<Suggestion>(loaded as Suggestion, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.`is`(tip.id))
        MatcherAssert.assertThat(loaded.tipFor, CoreMatchers.`is`(tip.tipFor))
        MatcherAssert.assertThat(loaded.tip, CoreMatchers.`is`(tip.tip))
    }
}



