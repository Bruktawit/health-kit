package com.example.healthkit.Data.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.healthkit.Data.MyDatabase
import com.example.healthkit.Data.Suggestion
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class SuggesionDaoTest {

    private lateinit var database: MyDatabase

    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database =
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                MyDatabase::class.java,
                "suggestion"
            ).allowMainThreadQueries().build()


    }

    @After
    fun closeDb() = database.close()


    @Test
    fun getSuggesionByTagAndSaveSuggesion_test() = runBlocking {
        // GIVEN - insert a group
        val suggestion = Suggestion(23, "Normal", "You should keep your health")
        database.suggestionDao().saveSuggestion(suggestion.tag)

        // WHEN - Get the group by code from the database
        val loaded = database.suggestionDao().getSuggestionByTag(suggestion.tag)

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat<Suggestion>(loaded as Suggestion, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.`is`(suggestion.id))
        MatcherAssert.assertThat(loaded.tag, CoreMatchers.`is`(suggestion.tag))
        MatcherAssert.assertThat(loaded.description, CoreMatchers.`is`(suggestion.description))
    }
}



