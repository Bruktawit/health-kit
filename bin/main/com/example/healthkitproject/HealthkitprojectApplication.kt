package com.example.healthkitproject

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HealthkitprojectApplication

fun main(args: Array<String>) {
	runApplication<HealthkitprojectApplication>(*args)
}
