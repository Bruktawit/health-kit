package com.example.healthkitproject

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api/reminders", produces = ["application/json"])
class ReminderController(private val repository: ReminderRepository) {

    @GetMapping("/")
    fun findAll():List<Reminder> = repository.findAll()
    
  

 
}