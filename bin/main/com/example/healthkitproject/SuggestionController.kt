package com.example.healthkitproject

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api/suggestions", produces = ["application/json"])
class SuggestionController(private val repository: CourseRepository) {

    @GetMapping("/")
    fun findAll():List<Suggestion> = repository.findAll()
    
    @GetMapping("/{tag}")
    fun findByTag(@PathVariable tag: String): ResponseEntity<Suggestion>{
       return repository.findByTag(tag).map {
           suggestion -> ResponseEntity.ok(suggestion)
       }.orElse(ResponseEntity.notFound().build())
    }

 
}