package com.example.healthkitproject

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api/rate", produces = ["application/json"])
class RateController(private val repository: RateRepository) {

    @PostMapping(path = "/new", consumes = "application/json", produces = "application/json")
    fun saveRate(@RequestBody rate: Rate){
       repository.save(rate)
    }
    
    @DeleteMapping(value = "/{userRate}")
    fun deleteRate(@PathVariable Int userRate) {

        repository.deleteByUserRate(userRate);

        
    }

}