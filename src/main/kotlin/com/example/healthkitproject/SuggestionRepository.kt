package com.example.healthkitproject

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CourseRepository: JpaRepository<Suggestion,Long> {
    fun findByTag(tag:String): Optional<Suggestion>
}