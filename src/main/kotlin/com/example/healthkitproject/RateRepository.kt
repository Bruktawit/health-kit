package com.example.healthkitproject

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RateRepository: JpaRepository<Rate,Long> {
    
    
    fun deleteByUserRate(rate:Int)
}