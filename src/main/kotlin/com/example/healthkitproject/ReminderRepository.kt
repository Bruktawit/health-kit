package com.example.healthkitproject

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ReminderRepository: JpaRepository<Suggestion,Long> {
  
}