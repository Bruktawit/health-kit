package com.example.healthkitproject

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TipRepository: JpaRepository<Tip,Long> {
    fun findByTipFor(tipFor:String): Optional<Tip>
}