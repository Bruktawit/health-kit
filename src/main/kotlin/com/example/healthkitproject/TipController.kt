package com.example.healthkitproject

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api/tips", produces = ["application/json"])
class TipController(private val repository: TipRepository) {
    
    @GetMapping("/{tipFor}")
    fun findByTipFor(@PathVariable tipFor: String): ResponseEntity<Tip>{
       return repository.findByTipFor(tipFor).map {
           tip -> ResponseEntity.ok(tip)
       }.orElse(ResponseEntity.notFound().build())
    }

 
}